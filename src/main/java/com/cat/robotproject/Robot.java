/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.robotproject;

import static java.lang.Math.abs;

/**
 *
 * @author Black Dragon
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = 'N';
    
    public Robot(int x, int y, int bx, int by, int N){
        this.N = N;
        if(!inMap(x,y)){
            this.x = 0;
            this.y = 0;
        }else{
            this.x = x;
            this.y = y;
        }
        if(!inMap(x,y)){
            this.bx = 0;
            this.by = 0;
        }else{
            this.bx = bx;
            this.by = by;
        }
    }
    
    public boolean walk(char direction){
        lastDirection = direction;
        switch(direction){
            case 'N':
                if(!inMap(x , y-1)){
                    printUnableToMove();
                    return false;
                }
                y -= 1;
                break;
            case 'S':
                if(!inMap(x , y+1)){
                    printUnableToMove();
                    return false;
                }
                y += 1;
                break;
            case 'E':
                if(!inMap(x+1 , y)){
                    printUnableToMove();
                    return false;
                }
                x += 1;
                break;
            case 'W':
                if(!inMap(x-1 , y-1)){
                    printUnableToMove();
                    return false;
                }
                x -= 1;
                break;
        }
        if(isBomb()){
            printBomb();
        }
        return true;
    }
    
    public boolean walk(char direction, int step){
        for(int i=0; i<step; i++){
            if(!this.walk(direction)){
                return false;
            }
        }
        return true;
    }
    
    public boolean walk(){
        return this.walk(lastDirection);
    }
    
    public boolean walk(int step){
        return this.walk(lastDirection , step);
    }
    
    public boolean inMap(int x, int y){
        if(x>=N || x<0 || y>=N || y<0){
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        return "Robot:(" + this.x + "," + this.y + ") Away form bomb("+ Math.abs(bx-x) + "," + Math.abs(by-y) + ")";
    }
    
    public void printUnableToMove(){
        System.out.println("I can't move!!!");
    }
    
    public boolean isBomb(){
        return x == bx && y == by;
    }
    
    public void printBomb(){
        System.out.println("Bomb found.");
    }
}
